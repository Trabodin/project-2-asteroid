﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour {
	public float MoveSpeed;
	public float TurnSpeed;
    public Transform tf;


	// Use this for initialization
	void Start () {
        //sets this as player in the game manager
		GameManager.instance.player = this;
        //gets the transform component
        tf = GameManager.instance.player.transform;
	}

	
	// Update is called once per frame
	void Update () {

        // code to rotate the player
        if(Input.GetKey(KeyCode.LeftArrow)|| Input.GetKey(KeyCode.RightArrow))
        {
            // gets the axis value then fips it to go the right way
            float dir = -(Input.GetAxis("Horizontal"));
            tf.transform.Rotate(new Vector3(0, 0, dir * TurnSpeed)); 
          
        }

        //coed to move forward and back
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow))
        {
          
            float spe = Input.GetAxis("Vertical");
          
			tf.position += (tf.rotation * Vector3.up) * spe * MoveSpeed; 
            //turns the vector the same amount as the ship
		
        }

		if (Input.GetKeyDown (KeyCode.Space)) {
            //creates a bullet at the players postion
			GameObject bullet = (GameObject)Instantiate (GameManager.instance.bullet, tf.position , Quaternion.identity);
		}
    }

    void OnCollisionEnter2D(Collision2D other)
    {
		if (other.gameObject.tag == "Asteroid") {
			
			GameManager.instance.lives = GameManager.instance.lives - 1;

            tf.transform.position = new Vector3(0, 0, 0);
			Debug.Log (GameManager.instance.lives);
		}

    }

	void OnTriggerExit2d(Collider2D other)
	{
		if (other.gameObject.tag == "Board") {
			GameManager.instance.lives = GameManager.instance.lives - 1;
            tf.transform.position = new Vector3(0, 0, 0);
        }

		
	}

}


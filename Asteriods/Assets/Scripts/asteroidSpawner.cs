﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class asteroidSpawner : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        //spans untill the max number of 
		if ((GameManager.instance.enemies.Count + GameManager.instance.enemyships.Count) < GameManager.instance.maxEnemies)
        {
            
            //gets a random spawn point for the type of asteroid and the spawn point to use
            int spawnNum = Random.Range(0, GameManager.instance.spawnPoints.Count);
            int randAsteroid = Random.Range(0, GameManager.instance.asteroids.Count);
            float randSS = Random.Range(0, GameManager.instance.spaceShipChance);
         
            Mathf.Round(randSS);
       
            GameObject spawnAsteroid = GameManager.instance.asteroids[randAsteroid];
            GameObject spawnPoint = (GameManager.instance.spawnPoints[spawnNum]);

            Vector3 circle = Random.insideUnitCircle;
            spawnPoint.transform.position = spawnPoint.transform.position + circle;

            //spawns the randomly solected asteroid at the randomly selected spawn point

            if (randSS == 1)
            {
                GameObject enemySpaceShip = (GameObject)Instantiate(GameManager.instance.enemySS, spawnPoint.transform.position, Quaternion.identity);
            }
            else
            {
                GameObject asteroid = (GameObject)Instantiate(spawnAsteroid, (spawnPoint.transform.position), Quaternion.identity);
            }
                

        }
	}
}

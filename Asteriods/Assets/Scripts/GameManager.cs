﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	// makes a Gamemanager variable
	public static GameManager instance;

	//set a variable in the game manager fo all the other scripts
	public player player;
	public List<enemy> enemies;
    public int spaceShipChance;
    public List<enemyship> enemyships;
	public List<GameObject> asteroids;
    public GameObject enemySS;
    public float enemySSMove;
    public int maxEnemies;
    public float asteroidMoveSpeed;
	public List<GameObject> spawnPoints;
	public float score;
	public int lives = 3;
	public GameObject bullet;
    public float bulletLifeTime;

	// Use this for initialization
	void Awake () {
		// makes sure theres only one
		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (gameObject);
		}
		else
		{
			Destroy (gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (lives <= 0)
        {

            DoGameOver();
        }

	}
	void DoGameOver()
	{

        Application.Quit();
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyship : MonoBehaviour {
    public Vector3 direction;


	// Use this for initialization
	void Start () {

        GameManager.instance.enemyships.Add(this);

	}
	
	// Update is called once per frame
	void Update () {
            
       //gets the vector to the ship and gets the angle to it and rotates the enemy ship that way
        direction = ((GameManager.instance.player.transform.position - transform.position));
        direction.Normalize();
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

        transform.rotation = Quaternion.Euler(0f, 0f, angle);
        transform.position += (transform.rotation* Vector3.right) * GameManager.instance.enemySSMove;


    }
    // destroys it if it collides with anything
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            OnDestory();
        }

    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            OnDestory();
        }

    }

    //destorys them when they leave the game area
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Board")
        {
            OnDestory();

        }

    }

    void OnDestory()
    {
        // removes them from the game manager list of enemies
        GameManager.instance.enemyships.Remove(this);
        //removes the game object
        Destroy(this.gameObject);
    }
}

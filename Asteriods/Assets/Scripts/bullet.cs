﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour {
	public float bulletSpeed;
	private Transform tf;
    Vector3 dirVector;

	// Use this for initialization
	void Start () {
		tf = this.gameObject.transform;
		
        //get thevector tward the player
        dirVector = (GameManager.instance.player.transform.rotation * Vector3.up);

        //destroys it after a  set time
        Destroy(this.gameObject, GameManager.instance.bulletLifeTime);

    }
	// Update is called once per frame
	void Update () {
        //get  vector direction twards the player when its spawned and mves it that way
        tf.position += dirVector * bulletSpeed;


    }
    //destroys it if it leaves the game board
	void OnTriggerExit2D(Collider2D other)
	{
		if (other.gameObject.tag == "Board")
		{
			OnDestroy ();
		}
	}
    //destroys it if it collides with an asteroid
	void OnTriggerEnter2D(Collider2D other)
	{
		
		if (other.gameObject.tag == "Asteroid") 
		{
			Destroy (other.gameObject);
			OnDestroy ();
		}

	}
	
    //function for destroying
	void OnDestroy()
	{
		Destroy (this.gameObject);
	}

}


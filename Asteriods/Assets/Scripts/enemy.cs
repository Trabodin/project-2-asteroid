﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy : MonoBehaviour {

    private Transform tf;
    Vector3 targeting;

    // Use this for initialization
    void Start () {
		// adds enemies to a list on enemies in the game manager
		GameManager.instance.enemies.Add (this);

        tf = GetComponent<Transform>();

        //gets the vector to the player when the object is created
        targeting = (GameManager.instance.player.transform.position - tf.position);
        targeting = Vector3.Normalize(targeting);
	}
	
	// Update is called once per frame
	void Update () {
      
            tf.position += targeting * .1f * GameManager.instance.asteroidMoveSpeed;
     

    }

    // destroys it if it collides with anything
    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            OnDestory();
        }
       
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            OnDestory();
        }

    }

    //destorys them when they leave the game area
    void OnTriggerExit2D(Collider2D other)
	{
		if (other.gameObject.tag == "Board") {
			OnDestory ();
           
		}

	}



    void OnDestory()
	{
		// removes them from the game manager list of enemies
		GameManager.instance.enemies.Remove (this);
        //removes the game object
        Destroy(this.gameObject);
    }
}
